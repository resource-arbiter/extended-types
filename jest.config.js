module.exports = {
  collectCoverage: true,
  collectCoverageFrom: ['src/**/*'],
  preset: 'ts-jest',
  testEnvironment: 'node'
}
