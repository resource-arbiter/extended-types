export type AnyIdentity =
  | number
  | string
  | symbol
  | Array<number | string | symbol>
  | {}

export type Identity<Type extends AnyIdentity> = Type
