import { AnyIdentity } from './Identity'

export interface Identifiable<Identity extends AnyIdentity> {
  id: Identity
}
