export type ArgsType<Fn extends (...args: any[]) => any> = Fn extends (
  ...args: infer Args
) => any
  ? Args
  : never
