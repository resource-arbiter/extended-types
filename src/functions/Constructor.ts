export type Constructor<
  Type = any,
  ConstructorArgs extends any[] = any[]
> = new (...args: ConstructorArgs) => Type
