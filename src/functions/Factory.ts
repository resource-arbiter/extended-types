export type Factory<Type = any, FactoryArgs extends any[] = any[]> = (
  ...args: FactoryArgs
) => Type
