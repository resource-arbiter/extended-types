export type FirstType<Arr extends any[]> = Arr extends [
  infer FirstElement,
  ...any[]
]
  ? FirstElement
  : void

export type SecondType<Arr extends any[]> = Arr extends [
  any,
  infer SecondElement,
  ...any[]
]
  ? SecondElement
  : void
