export type Action<Type = any> = {
  type: Type
}
