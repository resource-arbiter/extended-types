import { Action } from './Action'

export type FluxAction<Type = any, Payload = any> = Action<Type> & {
  payload: Payload
}
