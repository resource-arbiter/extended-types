import { Action } from './Action'

export type Reducer<State = any, AcceptedAction extends Action = Action> = <
  ReceivedAction extends AcceptedAction
>(
  currentState: State,
  receivedAction: ReceivedAction
) => State
