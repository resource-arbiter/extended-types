module.exports = {
  /**
   * Lint code with eslint, fixing any found issue if possible, formats with
   * Prettier and lasty, stages the file for commit.
   */
  '*.ts': ['eslint --fix', 'prettier --write', 'git add']
}
